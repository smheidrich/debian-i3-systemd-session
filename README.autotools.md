# Initial autotools setup

Reminder to myself. Actual "source" files for autotools:

- `configure.ac`
- `Makefile.am`

To generate all the autotools files, run:

- `aclocal`
- `autoconf`
- `automake --add-missing --foreign`
  - `--foreign` to avoid having to provide files like NEWS, AUTHORS, ...
- all this could be done in one step via `autoreconf` (stupid name, suggests
  it's a variant of `autoconf`) but I don't know how to add the CLI switches
  for `automake`
