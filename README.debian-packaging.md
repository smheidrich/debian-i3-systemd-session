
- `dh_make` creates initial template under `debian/`
- if you want to recreate delete example files: `dh_make -a`
- `debuild -us -uc` builds package (written to `../package-name_ver_arch.deb`)
- to list contents: `dpkg -c package_path.deb`
